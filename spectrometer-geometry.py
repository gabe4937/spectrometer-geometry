import math

def intersection(x1,y1,x2,y2,x3,y3,x4,y4):
    px= ( (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) ) 
    py= ( (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
    return [px, py]
	
def rotate(x,y,xo,yo,theta): #rotate x,y around xo,yo by theta (rad)
    xr=math.cos(theta)*(x-xo)-math.sin(theta)*(y-yo)   + xo
    yr=math.sin(theta)*(x-xo)+math.cos(theta)*(y-yo)  + yo
    return [xr,yr]

def makeBeam(beamx1,beamy1,beamtheta,xdir,ydir):
	return [beamx1 + xdir*math.cos(beamtheta) , beamy1 + ydir*math.sin(beamtheta)]

print("")

#fiber
xf1=25.8
yf1=-46.0
xf2=32.0
yf2=-46.5
thetaf=math.pi/2-math.atan(abs(yf2-yf1)/abs(xf2-xf1))
print("thetaf: "+str(thetaf*180/math.pi))

beam1 = [(xf2-xf1)/2+xf1,  (yf1-yf2)/2+yf2]
beam2=makeBeam(beam1[0],beam1[1],thetaf,1,1)

#mirror
xm1=25.2
ym1=-18.0
xm2=37.1
ym2=-16.0
thetam=math.atan(abs(ym2-ym1)/abs(xm2-xm1))
print("thetam: "+str(thetam*180/math.pi))


beam1=intersection(beam1[0],beam1[1],beam2[0],beam2[1],xm1,ym1,xm2,ym2)
beam2=makeBeam(beam1[0],beam1[1],thetaf-thetam,1,-1)



#grating
xg1=35.0
yg1=-46.0
xg2=48.0
yg2=-44.0
thetag=math.atan(abs(yg2-yg1)/abs(xg2-xg1))
print("thetag: "+str(thetag*180/math.pi))



#add gt mm to glass
gt=3
xg1=xg1-gt*math.cos(math.pi/2-thetag) 
yg1=yg1+gt*math.sin(math.pi/2-thetag) 
xg2=xg2-gt*math.cos(math.pi/2-thetag)
yg2=yg2+gt*math.sin(math.pi/2-thetag) 



#rotation point of grating !measure these
thetago=20*math.pi/180
xgo=40
ygo=-46
thetag=thetag+thetago
print("thetag: "+str(thetag*180/math.pi))

#rotate
xgr1 = rotate(xg1,yg1,xgo,ygo,thetago)[0]
ygr1 = rotate(xg1,yg1,xgo,ygo,thetago)[1]
xgr2 = rotate(xg2,yg2,xgo,ygo,thetago)[0]
ygr2 = rotate(xg2,yg2,xgo,ygo,thetago)[1]
xg1=xgr1
yg1=ygr1
xg2=xgr2
yg2=ygr2



beam1=intersection(beam1[0],beam1[1],beam2[0],beam2[1],xg1,yg1,xg2,yg2)


#angle of incidence on grating
thetai=math.pi/2-(thetaf-2*thetam+thetag)
print("thetai: "+str(thetai*180/math.pi))
print("")

#wavelengths
lambdas=[] 
lambdas.append(300.0)
lambdas.append(400.0)
lambdas.append(700.0)
lambdas.append(1100.0)

#grating
d=(1*10**(-3))/600

#mirrorb
xmb1=22
ymb1=-21
xmb2=15.5
ymb2=-41

thetas=[]
spots=[] #spots on mirror

#spots
for n in range(5):
	print("ORDER: " + str(n))
	thetas.append([])
	spots.append([])
	for w in range(len(lambdas)):
		try:
			thetas[n].append( math.asin(n*lambdas[w]*10**(-9)/d - math.sin(thetai) ))
			beam2=makeBeam(beam1[0],beam1[1],math.pi/2-abs(thetas[n][w])-thetag,-thetas[n][w]/abs(thetas[n][w]),1)
			spots[n].append(intersection(beam1[0],beam1[1],beam2[0],beam2[1],xmb1,ymb1,xmb2,ymb2))
			print("Wavelength: " +str(lambdas[w])+", theta["+str(n)+"]: "+str(thetas[n][w]*180/math.pi)+", Spot: "+str(spots[n][w]))
		except:
			print("Wavelength: "+str(lambdas[w])+", Reflection not allowed")